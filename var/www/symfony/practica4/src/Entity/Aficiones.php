<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AficionesRepository")
 */
class Aficiones
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function __toString() {
         return $this->nom_aficion;
     }
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="aficion")
     */
    private $nombre_aficion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nom_aficion;

    public function __construct()
    {
        $this->nombre_aficion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    


    /**
     * @return Collection|User[]
     */
    public function getNombreAficion(): Collection
    {
        return $this->nombre_aficion;
    }

    public function addNombreAficion(User $nombreAficion): self
    {
        if (!$this->nombre_aficion->contains($nombreAficion)) {
            $this->nombre_aficion[] = $nombreAficion;
            $nombreAficion->addAficion($this);
        }

        return $this;
    }

    public function removeNombreAficion(User $nombreAficion): self
    {
        if ($this->nombre_aficion->contains($nombreAficion)) {
            $this->nombre_aficion->removeElement($nombreAficion);
            $nombreAficion->removeAficion($this);
        }

        return $this;
    }

    public function getNomAficion(): ?string
    {
        return $this->nom_aficion;
    }

    public function setNomAficion(?string $nom_aficion): self
    {
        $this->nom_aficion = $nom_aficion;

        return $this;
    }
}
