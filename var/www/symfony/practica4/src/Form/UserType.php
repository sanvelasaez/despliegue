<?php

namespace App\Form;

use App\Entity\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class)
            ->add('apellidos', TextType::class)

            ->add('fecha_nacimiento', DateType::class, array(
                'widget' => 'single_text'

            ))

            ->add('sexo', ChoiceType::class, array(
                'choices' => array(

                    'Hombre' => 'Hombre',
                    'Mujer' => 'Mujer',

                ),
                'expanded' => true,
                'multiple' => false,
                'label' => 'Sexo',
                'required' => true
            ))

            ->add('foto', FileType::class, [
                'label' => 'Foto',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/gif',
                        ],
                        'mimeTypesMessage' => 'Por favor introduce un formato de imagen válido',
                    ])
                ],
            ])
            ->add('email',EmailType::class)
            ->add('password', PasswordType::class)
            ->add('ciudad', EntityType::class, array(
                'required' => true,
                'label' => 'Ciudad de origen',
                'placeholder' => 'Selecciona ciudad...',
                'class' => 'App:Ciudades'
            ))
            ->add('aficion', null, [
                'multiple' => true,
                'expanded' => true,
                'label_attr' => ['class' => 'aficiones']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
