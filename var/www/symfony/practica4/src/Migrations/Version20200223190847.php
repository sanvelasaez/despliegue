<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223190847 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, ciudad_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, nombre VARCHAR(30) NOT NULL, apellidos VARCHAR(50) NOT NULL, fecha_nacimiento DATE NOT NULL, sexo VARCHAR(10) NOT NULL, foto VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D649E8608214 (ciudad_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_aficiones (user_id INT NOT NULL, aficiones_id INT NOT NULL, INDEX IDX_4F937A74A76ED395 (user_id), INDEX IDX_4F937A749A32A73 (aficiones_id), PRIMARY KEY(user_id, aficiones_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aficiones (id INT AUTO_INCREMENT NOT NULL, nom_aficion VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ciudades (id INT AUTO_INCREMENT NOT NULL, nombre_ciu VARCHAR(40) NOT NULL, UNIQUE INDEX UNIQ_FF77006A22E9141 (nombre_ciu), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E8608214 FOREIGN KEY (ciudad_id) REFERENCES ciudades (id)');
        $this->addSql('ALTER TABLE user_aficiones ADD CONSTRAINT FK_4F937A74A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_aficiones ADD CONSTRAINT FK_4F937A749A32A73 FOREIGN KEY (aficiones_id) REFERENCES aficiones (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_aficiones DROP FOREIGN KEY FK_4F937A74A76ED395');
        $this->addSql('ALTER TABLE user_aficiones DROP FOREIGN KEY FK_4F937A749A32A73');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649E8608214');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_aficiones');
        $this->addSql('DROP TABLE aficiones');
        $this->addSql('DROP TABLE ciudades');
    }
}
