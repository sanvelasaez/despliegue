======================================
=== FORMULARIO - SYMFONY -  LOGIN  ===
======================================

Contribuyentes: Santiago Velasco, Alba Moreno, Clara Alcazar
Version symfony: 5.0.4

=== Descripcion ===

- Webpage que permite añadir, editar, eliminar y mostrar usuarios.
- Contiene un formulario para que los usuarios se "registren".
- Contiene un login para que los administradores editen/eliminen usuarios, 
ciudades y aficiones

	Admin: sac@gmail.com
	Passwd: 1234